- 👋 Hi, I’m @rshowrav
- 👀 I’m interested in data science, data analytics projects with large datasets.
- 🌱 I’m currently learning advanced Python and R predictive modeling, computer vision, docker containers, and airflow
- 💞️ I’m looking to collaborate on almost any interesting data problem
- 📫 How to reach me by email at rshowrav@gmail.com 

<!---
rshowrav/rshowrav is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
